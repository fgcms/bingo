package bingo.util.beetl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.beetl.core.Context;
import org.beetl.core.Function;

public class ImportantTagFunction implements Function {

	public Object call(Object[] paras, Context ctx) {
		// TODO Auto-generated method stub
		if(paras[0]==null) return Collections.EMPTY_LIST;
		List<HTMLTag> list = (List<HTMLTag>)paras[0];
		List<HTMLTag> impList = new ArrayList<HTMLTag>();
		List<HTMLTag> notImpList = new ArrayList<HTMLTag>();
		for(HTMLTag tag :list){
			String attrValue = (String)tag.get("important");
			if(attrValue==null){
				notImpList.add(tag);
				continue;
			}
			if(Boolean.parseBoolean(attrValue)){
				impList.add(tag);
			}else{
				notImpList.add(tag);
			}
		}
		
		return new Object[]{impList,notImpList};
	}

}
