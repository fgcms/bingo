package bingo.filter;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.beetl.core.GroupTemplate;
import org.beetl.core.exception.ScriptEvalError;
import org.beetl.ext.servlet.ServletGroupTemplate;
import org.beetl.ext.web.SimpleCrossFilter;

import bingo.util.MobileUtil;
import bingo.util.beetl.TerminalWebRender;

/**
 * Servlet Filter implementation class HTMLFilter
 */
public class HTMLFilter extends SimpleCrossFilter implements Filter {

	public void init(FilterConfig arg0) throws ServletException {

	}

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse rsp = (HttpServletResponse) response;
		
		rsp.setContentType("text/html;charset=utf-8");
		MobileUtil.initMobileStatus(req);
		
		if (canProcceed(req, response)) {
			String path = req.getServletPath();
			String valueFile = getValueFile(path, req, rsp);
			GroupTemplate gt = getGroupTemplate();

			TerminalWebRender render = new TerminalWebRender(gt);
			Map paras = this.getScriptParas(req, rsp);

			String commonFile = getCommonValueFile(req, rsp);
			Map commonData = new HashMap(), data = new HashMap();
			try {
				if (gt.getResourceLoader().exist(commonFile)) {
					commonData = gt.runScript(commonFile, paras);

				}

				if (gt.getResourceLoader().exist(valueFile)) {
					data = gt.runScript(valueFile, paras);
				}

			} catch (ScriptEvalError e) {
				throw new RuntimeException("伪模型脚本有错！", e);
			}

			commonData.putAll(data);
			Iterator it = commonData.keySet().iterator();
			while (it.hasNext()) {
				String key = (String) it.next();
				Object value = commonData.get(key);
				setValue(key, value, req);
			}

			String renderPath = getRenderPath(path, req, rsp);

			render.render(renderPath, req, rsp);
		} else {

			chain.doFilter(request, response);
		}

	}

	@Override
	protected GroupTemplate getGroupTemplate() {
		return ServletGroupTemplate.instance().getGroupTemplate();
	}

}