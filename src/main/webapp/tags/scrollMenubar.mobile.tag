<div id="wrapper">
	<div id="scroller">
@var items = tag.children;
@var size = items.~size;
@for(menu in items){

<span>${menu.body}</span>
@}
	</div>
</div>

<script>
var myScroll;
$(document).ready(function(){
	$("#scroller").css("width",${size}*80);
	myScroll = new IScroll('#wrapper', { scrollX: true, scrollY: false, mouseWheel: true });
	
});

</script>
