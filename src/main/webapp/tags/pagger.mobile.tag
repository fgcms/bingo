@var data = tag.data; //pageView //假设从0行，0页开始
@var recNum = data.recNum,list = data.result,currPage = data.currPage,pageNum=data.pageNum,recNumPerPage=data.recNumPerPage;
@var startRec = currPage*recNumPerPage+1,endRec = startRec+list.~size-1; //起始行
@var hasPrevious = currPage!=0,hasNext = currPage!=pageNum-1; //当前页是否是起始页，结束页
@ //翻页的范围，todo
@ var url = data.url;
  <dd>
          <ul class="pagination rpage-ul">
            <li class="disabled"><a href="${url}" aria-label="Previous"><span aria-hidden="true">&larr;</span> 上一页</a></li>
            <li><a href="${url}" aria-label="Next">下一页 <span aria-hidden="true">&rarr;</span></a></li> 
          </ul>
          <div class="rpage-box">
              <span class="r-mar">共<strong class="blue1">${pageNum}</strong>页</span>
              <span>共 <strong class="blue1">${recNum}</strong> 条 </span>
          </div>
</dd>