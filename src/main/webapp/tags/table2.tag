 @var trs = tag.children, check=(tag.check!"false")=="true";
 
 <dd class="table-responsive">
            <table class="table table-striped" id="${tag.id}">
              <thead>
                <tr>
                 @if(check){                 	
                  <th width="13">
                    <input type="checkbox" id="chk_all"/>
                  </th>
                  @} for(tr in trs){
                  <th>${tr.name}</th>
                  @}
                  
                </tr>
              </thead>
              <tbody>
              	@for(row in tag.data){
              	@	  @tag.binds(row,rowLP.index);          	
                <tr>
                	@if(check){
                  <td>
                    <input type="checkbox" name="No_select" />
                  </td>
                   @}for(tr in trs){                 
                  <td>${tr.body}</td>
                   @}
                </tr>
                @}
             
              </tbody>
            </table>
        
        </dd>
       