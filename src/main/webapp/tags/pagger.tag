@var data = tag.data; //pageView //假设从0行，0页开始
@var recNum = data.recNum,list = data.result,currPage = data.currPage,pageNum=data.pageNum,recNumPerPage=data.recNumPerPage;
@var startRec = currPage*recNumPerPage+1,endRec = startRec+list.~size-1; //起始行
@var hasPrevious = currPage!=0,hasNext = currPage!=pageNum-1; //当前页是否是起始页，结束页
@ //翻页的范围，todo
@ var url = data.url;
<dd id="${tag.id}">
      	<!--  翻页 -->
          <div class="nobor">每页显示 <span class="blue1">${startRec}-${endRec}</span> 条，共 <span class="blue1">${ recNum} </span> </div>
          	
          <div class="rpage-box">
              <span class="r-mar">共<strong class="blue1">${pageNum} </strong>页</span>
              <span>去第&nbsp;</span>
              <input type="text" name="pages" class="form-control intxt5" value="" />
              <span>&nbsp;页</span>
          </div>
          <ul class="pagination rpage-ul">
            <li ${currPage==0?'class="disabled"'}><a href="javascript:void(0)" page="1"><span aria-hidden="true">首页</span></a></li>
            <li ${!hasPrevious?'class="disabled"'}><a href="2" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
            @ for(var i=0;i<pageNum;i++){
            	@if(i==currPage){
            <li class="active"><a href="javascript:void(0)" page="${i+1}">${i+1}  <span class="sr-only">(current)</span></a></li>
            	@}else{
            <li><a href="javascript:void(0)" page="${i+1}">${i+1}</a></li>
            	@}
            @}
              <li ${!hasNext?class="disabled"} ><a href="#" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>
            <li><a href="#"><span aria-hidden="true">末页</span></a></li>
          </ul>
              
        </dd>
        <script>
        
     	$("#${tag.id}").pagger({
     		formId:"${tag.form}",         		
     		ajaxTarget:"${tag.ajaxTarget}"
     			
     	})
     	
        </script>