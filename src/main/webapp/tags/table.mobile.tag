@var children = tag.children;//获得下级标签
@group = getImportantTags(children);
@var trs=group[0];
@var hiddenTrs = group[1];
@for(row in tag.data) {
@	@tag.binds(row,rowLP.index);
@	for(tr in trs){
@		if(tr.tagName=="tr"){
<div>
@			if(trLP.index<=1){
<h1>${tr.body}</h1>
@			}else{ 
${tr.name}:${tr.body}
@			}
@		}else if(tr.tagName=="trOpt"){
${tr.execute}
@		} 
</div>

@	}
<div class="showMore"><a href="#">更多.....</a></div>
<div style="display:none">
@	for(tr in hiddenTrs){
<span>${tr.name}:${tr.body}</span>
@	}
</div>
@}

<script>

(function($){ 
	$.fn.showButtons = function(){
	var top = $(window).height();
	var left = $(window).width();
	
	this.css( { width:'100%',position : 'absolute', 'top' : top , left : 0} ).show();
	this.animate({	   
	    top:'-=50px'
	   
	  },1000); 
	 
	 
	}
	})(jQuery)


$(document).ready(function(){
	
	
	$(".showMore").on("click",function(){	
		$(this).next().toggle();			
	})
	
	$(".trOpt").on("click",function(){	
		var opt = $(this).find(".trOptBody").get(0)	;
		var newDiv = $("<div class='buttons'>"+$(opt).html()+"</div>");
		$("#bottomButtons").html("<div class='buttons' style='height:50px,border:1px solid black;'>"+$(opt).html()+" | <a href=‘#’>关闭</a></div>");
		$("#bottomButtons").showButtons();
	})
	
	
});

</script>


